# create-graphql-node

## Development

```bash
docker-compose build
docker-compose run --rm app npm-install-peers
docker-compose run --rm app npm run build
```

## License

MIT

## Author

Tim Kolberger
[<img src="https://simpleicons.org/icons/gmail.svg" width="24"/>](mailto:tim@kolberger.eu)
[<img src="https://simpleicons.org/icons/twitter.svg" width="24"/>](https://twitter.com/TimKolberger)
