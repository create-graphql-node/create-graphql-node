import * as path from 'path';
import * as fs from 'fs';
import * as shell from 'shelljs';
import chalk from 'chalk';
import * as Generator from 'yeoman-generator';
import * as ora from 'ora';
import { graphQlLogo } from './graphQLLogo';

const tic = chalk.green('✓');
const tac = chalk.red('✗');

class AppGenerator extends Generator {
    private spinner: any;
    private readonly dir: string;
    options: { name: string };
    private async: () => Function;

    constructor(args: string | string[], options: object) {
        super(args, options);

        this.argument('name', {
            type: String,
            required: true,
        });

        this.dir = path.resolve(this.options.name);
    }

    initializing() {
        this.spinner = ora();

        this._printGraphQLLogo();
    }

    cloneStarterCode() {
        this.spinner.start();

        this._validateDirectory();

        this.spinner.text = 'Creating a new GraphQL project...';

        const repository = 'https://gitlab.com/create-graphql-node/boilerplate-ts.git';

        const done = this.async();
        const command = 'git';
        const commandOpts = ['clone', repository, this.dir];
        const checkoutCommandOpts = ['checkout', 'master'];

        this.spawnCommand(command, commandOpts, { stdio: 'ignore' }).on('close', () => {
            shell.cd(this.dir);

            this.spawnCommand(command, checkoutCommandOpts, { stdio: 'ignore' }).on('close', () => {
                this.spinner.stop();

                this.log(`${tic} GraphQL project ${this.options.name} created.`);

                done();
            });
        });
    }

    _printGraphQLLogo() {
        this.log(graphQlLogo);
    }

    _validateDirectory() {
        try {
            fs.lstatSync(this.dir).isDirectory();

            this._logAndExit(
                `${tac} Directory "${this.options.name}" already exists,
        please enter a new directory name or delete "${this.options.name}"!`,
            );

            return false;
        } catch (err) {
            return true;
        }
    }

    installModules() {
        shell.cd(this.dir);

        this.spinner.start();

        this.spinner.text = 'Installing dependencies...';

        const done = this.async();
        const command = './cli';
        const args = ['setup'];

        this.spawnCommand(command, args, { stdio: 'inherit' }).on('close', () => {
            this.spinner.stop();
            done();
        });
    }

    _cleanDir() {
        shell.cd(this.dir);

        shell.rm('-rf', '.git');
    }

    _logAndExit(message: string) {
        this.spinner.stop();

        this.log(message);

        process.exit(1);
    }

    end() {
        this._cleanDir();

        this.log(`${tic} Your new project with GraphQL has been created! 🔥`);
    }
}

module.exports = AppGenerator;
