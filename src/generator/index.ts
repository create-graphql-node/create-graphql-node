import * as program from 'commander';
import { verifyYeoman } from './installYeoman';
import { create } from './init';

let version = 'none';

try {
    const packagejson = require('../../package.json');
    version = packagejson.version;
} catch (e) {
    console.warn(`No package.json found. Version is set to '${version}'`);
}

program.version(version);

program
    .command('init <project>')
    .alias('i')
    .description('Create a new GraphQL project')
    .action(async project => {
        await verifyYeoman();

        create(project);
    });

program.parse(process.argv);

if (process.argv.length <= 2) {
    program.help();
}
