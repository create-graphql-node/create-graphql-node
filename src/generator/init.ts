import * as spawn from 'cross-spawn';
import * as path from 'path';

export function create(project: string) {
    const spawnOptions = [path.join(__dirname, 'app.js'), project];

    spawn('yo', spawnOptions, { shell: true, stdio: 'inherit' });
}
