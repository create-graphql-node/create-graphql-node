import { Filter, Query } from '..';
import { SelectQueryBuilder } from 'typeorm/query-builder/SelectQueryBuilder';
import { BaseEntity } from 'typeorm';
import FilterOperator = Query.FilterOperator;
import LogicFilterOperator = Query.LogicFilterOperator;
import QueryFilter = Query.QueryFilter;
import FilterParent = Filter.FilterParent;
import IFilterContext = Filter.IFilterContext;

/**
 * Defining a filter like:
 *
 * input PostFilter {
 *   or: [PostFilter]
 *   and: [PostFilter]
 *   id: IntegerFilter
 *   content: StringFilter
 *   done: BooleanFilter
 *   likes: IntegerFilter
 * }
 *
 * Allows to run queries like:
 *
 * allPosts(filter: {
 *   or: [
 *     {
 *       content: {
 *         like: "b"
 *       },
 *       done: {
 *         eq: false
 *       }
 *     },
 *     {
 *       content: {
 *         like: "a"
 *       }
 *     }
 *   ],
 *   and: {
 *     likes: { lte: 13 }
 *   }
 * })
 *
 * }) { edges { node { id } } }
 *
 */

type OperatorCreator = (fieldName: string) => string;
const filterOperatorMap: { [key: string]: OperatorCreator } = {
    [FilterOperator.eq]: fieldName => `= :${fieldName}`,
    [FilterOperator.ne]: fieldName => `!= :${fieldName}`,
    [FilterOperator.gt]: fieldName => `> :${fieldName}`,
    [FilterOperator.gte]: fieldName => `>= :${fieldName}`,
    [FilterOperator.lt]: fieldName => `< :${fieldName}`,
    [FilterOperator.lte]: fieldName => `<= :${fieldName}`,
    [FilterOperator.in]: fieldName => `IN (:${fieldName})`,
    [FilterOperator.nin]: fieldName => `NOT IN (:${fieldName}})`,
    [FilterOperator.regex]: fieldName => `~ :${fieldName}`,
    [FilterOperator.like]: fieldName => `~* :${fieldName}`,
};

const logicOperatorMap: { [key: string]: (qb: SelectQueryBuilder<BaseEntity>) => Function } = {
    [LogicFilterOperator.or]: qb => qb.orWhere.bind(qb),
    [LogicFilterOperator.and]: qb => qb.andWhere.bind(qb),
};

const logicOperatorMapKeys = Object.keys(logicOperatorMap);

const isReservedKeyWord = (key: string) =>
    Object.keys(filterOperatorMap).includes(key) || Object.keys(logicOperatorMap).includes(key);

export function applyToQueryBuilder(
    filters: QueryFilter,
    filterFieldName: string,
    parent: Filter.FilterParent,
    filterValue: any,
    queryBuilder: SelectQueryBuilder<BaseEntity>,
    logicOperatorFn: Function,
) {
    if (logicOperatorMapKeys.includes(filterFieldName)) {
        const logicOperatorFn = logicOperatorMap[filterFieldName](queryBuilder);
        applyFilters(queryBuilder, filters[filterFieldName] as QueryFilter, logicOperatorFn);
        return;
    } else if (Object.keys(filterOperatorMap).includes(filterFieldName)) {
        const parentOrFieldName = parent ? parent.name : filterFieldName;
        const operator = filterOperatorMap[filterFieldName](parentOrFieldName);
        logicOperatorFn(`${parentOrFieldName} ${operator}`, {
            [parentOrFieldName]: filterValue,
        });
        return;
    }
}

export function applyFilters(
    queryBuilder: SelectQueryBuilder<BaseEntity>,
    filters?: QueryFilter,
    logicOperatorFn: Function = queryBuilder.where.bind(queryBuilder),
    parent?: FilterParent,
) {
    if (!filters || typeof filters !== 'object') {
        return;
    }

    if (Array.isArray(filters)) {
        filters.forEach(filter => applyFilters(queryBuilder, filter, logicOperatorFn, parent));
        return;
    }

    Object.keys(filters).forEach((filterFieldName, index) => {
        if (index === 1) {
            logicOperatorFn = queryBuilder.andWhere.bind(queryBuilder);
        }

        const filter = filters[filterFieldName];

        if (isReservedKeyWord(filterFieldName)) {
            const filterValue = filters[filterFieldName];
            applyToQueryBuilder(
                filters,
                filterFieldName,
                parent,
                filterValue,
                queryBuilder,
                logicOperatorFn,
            );
            return;
        }

        if (Array.isArray(filter)) {
            filter.forEach(f =>
                applyFilters(queryBuilder, f, logicOperatorFn, {
                    filters,
                    name: filterFieldName,
                }),
            );
            return;
        }

        if (filter && typeof filter === 'object') {
            applyFilters(queryBuilder, filter as QueryFilter, logicOperatorFn, {
                filters: filters,
                name: filterFieldName,
            });
        }
    });
}

export function filter(rootValue: any, args: { filter: QueryFilter }, context: IFilterContext) {
    applyFilters(context.queryBuilder, args.filter);
    return context.queryBuilder;
}
