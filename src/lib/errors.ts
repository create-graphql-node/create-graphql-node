import { createError } from 'apollo-errors';

export const NotFoundError = createError('NotFoundError', {
    message: 'Not found',
});
