import { Generator, IContext, ResolverFn } from '..';
import ICUDOptions = Generator.ICUDOptions;
import IMutatorOptions = Generator.IMutatorOptions;
import mutatorFn = Generator.mutatorFn;
import { NotFoundError } from './errors';
import IBasicResolverNames = Generator.IBasicResolverNames;
import { merge } from 'lodash';

export function compose(...fns: Function[]) {
    return fns.reduceRight(
        (prevFn, nextFn) => (value: any) => nextFn(prevFn(value)),
        (value: any) => value,
    );
}

export function withName(name: string) {
    return function(target: object) {
        return Object.entries(target).reduce(
            (acc, [key, resolverFn]) => ({ ...acc, [`${key}${name}`]: resolverFn }),
            {},
        );
    };
}

export function just(scopes: string | string[]) {
    return (target: object) =>
        Object.entries(target).reduce(
            (acc, [key, resolverFn]) => {
                if (
                    (Array.isArray(scopes) && !scopes.includes(key)) ||
                    (!Array.isArray(scopes) && scopes !== key)
                ) {
                    return acc;
                }

                return {
                    ...acc,
                    [key]: resolverFn,
                };
            },
            {} as { [key in IBasicResolverNames]: ResolverFn },
        );
}

interface IBasicResolver<T = any> {
    create?: ResolverFn<T>;
    update?: ResolverFn<T>;
    remove?: ResolverFn<T>;
}

async function defaultMutator(
    model: Function,
    entity: any,
    { [model.name.toLowerCase()]: input }: { [key: string]: any },
    options: IMutatorOptions,
) {
    await Promise.all(
        Object.entries(input).map(async ([key, value]) => {
            if (options.keyBlackList.includes(key)) {
                return;
            }

            if (typeof options.mapper !== 'function') {
                entity[key] = value;
                return;
            }

            const mapped = await options.mapper(value, key, entity);
            if (mapped !== undefined) {
                entity[key] = mapped;
            }
        }),
    );
}

async function defaultMapper(value: any, field: string, entity: { [key: string]: any }) {
    if (typeof entity[field] === 'function') {
        await entity[field](value);
        return;
    }

    return value;
}

const defaultMutators: {
    create: { fn: mutatorFn; options?: IMutatorOptions };
    update: { fn: mutatorFn; options?: IMutatorOptions };
} = {
    create: { fn: defaultMutator, options: { keyBlackList: [] } },
    update: { fn: defaultMutator, options: { keyBlackList: ['id'] } },
};

const defaultMappers = {
    create: defaultMapper,
    update: defaultMapper,
};

export function generateMutations(model: Function, options?: ICUDOptions): IBasicResolver {
    const { hooks, mutators, mappers } = merge(
        {
            hooks: {
                preCreate: null,
                postCreate: null,
                preUpdate: null,
                postUpdate: null,
                preRemove: null,
                postRemove: null,
            },
            mutators: defaultMutators,
            mappers: defaultMappers,
        },
        options || {},
    );

    return {
        create: async (rootValue, args, context, info) => {
            const repository = context.connection.getRepository(model);
            const entity = repository.create();

            if (typeof hooks.preCreate === 'function') {
                await hooks.preCreate(entity, rootValue, args, context, info);
            }

            await mutators.create.fn(model, entity, args, {
                mapper: mappers.create,
                ...mutators.create.options,
            });
            const savedEntity = await repository.save(entity);

            if (typeof hooks.postCreate === 'function') {
                await hooks.postCreate(savedEntity, rootValue, args, context, info);
            }

            return savedEntity;
        },
        update: async (rootValue, args, context, info) => {
            const { id } = args;
            const repository = context.connection.getRepository(model);
            const entity = await repository.findOne(id);

            if (!entity) {
                throw new NotFoundError(`Could not find ${model.name} for id ${id}`);
            }

            if (typeof hooks.preUpdate === 'function') {
                await hooks.preUpdate(entity, rootValue, args, context, info);
            }

            await mutators.update.fn(model, entity, args, {
                mapper: mappers.update,
                ...mutators.update.options,
            });
            const savedEntity = await repository.save(entity);

            if (typeof hooks.postUpdate === 'function') {
                await hooks.postUpdate(savedEntity, rootValue, args, context, info);
            }

            return savedEntity;
        },
        remove: async (rootValue, args, context: IContext, info) => {
            const {
                [model.name.toLowerCase()]: { id },
            } = args;
            const repository = context.connection.getRepository(model);
            const entity = await repository.findOne(id);

            if (!entity) {
                throw new NotFoundError(`Could not find ${model.name} for id ${id}`);
            }

            if (typeof hooks.preRemove === 'function') {
                await hooks.preRemove(entity, rootValue, { id }, context, info);
            }

            const removedEntity = await repository.remove(entity);

            if (typeof hooks.postRemove === 'function') {
                await hooks.postRemove(removedEntity, rootValue, { id }, context, info);
            }

            return { ...entity, id };
        },
    };
}
