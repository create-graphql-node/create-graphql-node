import { BaseEntity, Repository, SelectQueryBuilder } from 'typeorm';
import { IContext, IObject, Query } from '..';
import IPaginationArguments = Query.IPaginationArguments;

const PREFIX = 'connection:';

export const base64 = (str: string) => new Buffer(str, 'ascii').toString('base64');
export const unbase64 = (b64: string) => new Buffer(b64, 'base64').toString('ascii');

/**
 * Rederives the offset from the cursor string
 */
export function cursorToOffset(cursor: string) {
    return parseInt(unbase64(cursor).substring(PREFIX.length), 10);
}

/**
 * Given an optional cursor and a default offset, returns the offset to use;
 * if the cursor contains a valid offset, that will be used, otherwise it will
 * be the default.
 */
export function getOffsetWithDefault(cursor: string, defaultOffset: number) {
    if (cursor === undefined) {
        return defaultOffset;
    }
    const offset = cursorToOffset(cursor);
    return isNaN(offset) ? defaultOffset : offset;
}

/**
 * Creates the cursor string from an offset.
 */
export function offsetToCursor(offset: number) {
    return base64(PREFIX + offset);
}

interface IPaginateContext extends IContext {
    repository: Repository<BaseEntity>;
    queryBuilder?: SelectQueryBuilder<BaseEntity>;
    mapper?: (value: BaseEntity, index: number, array: BaseEntity[]) => any;
}

/**
 * Accepts a mongoose query and connection arguments, and returns a connection
 * object for use in GraphQL. It uses array offsets as pagination, so pagination
 * will work only if the data set is static.
 */
export async function paginate(
    rootValue: any,
    args: IPaginationArguments,
    context: IPaginateContext,
) {
    const { mapper, repository, queryBuilder = repository.createQueryBuilder() } = context;
    const { after, before, first, last, sort }: IPaginationArguments = args;
    const totalCount = await repository.count();
    const beforeOffset = getOffsetWithDefault(before, totalCount);
    const afterOffset = getOffsetWithDefault(after, -1);

    let startOffset = Math.max(-1, afterOffset) + 1;
    let endOffset = Math.min(totalCount, beforeOffset);

    if (first) {
        endOffset = Math.min(endOffset, startOffset + first);
    }
    if (last) {
        startOffset = Math.max(startOffset, endOffset - last);
    }

    const skip = Math.max(startOffset, 0);
    const limit = endOffset - startOffset;

    if (Array.isArray(sort)) {
        const typeOrmSort = sort.reduce(
            (sortObject, s) => ({
                ...sortObject,
                [s.field]: s.order.toLowerCase(),
            }),
            {} as IObject,
        );
        queryBuilder.orderBy(typeOrmSort);
    }

    // If supplied slice is too large, trim it down before mapping over it.
    queryBuilder.skip(skip).take(limit);

    // Short circuit if limit is 0; in that case, mongodb doesn't limit at all
    let slice: BaseEntity[] = [];
    let count: number = 0;
    if (limit > 0) {
        [slice, count] = await queryBuilder.getManyAndCount();
    }

    // If we have a mapper function, map it!
    if (typeof mapper === 'function') {
        slice = slice.map(mapper);
    }

    const edges = slice.map((value, index) => ({
        cursor: offsetToCursor(startOffset + index),
        node: value,
    }));

    const firstEdge = edges[0];
    const lastEdge = edges[edges.length - 1];
    const lowerBound = after ? afterOffset + 1 : 0;
    const upperBound = before ? Math.min(beforeOffset, totalCount) : totalCount;

    return {
        edges,
        pageInfo: {
            startCursor: firstEdge ? firstEdge.cursor : null,
            endCursor: lastEdge ? lastEdge.cursor : null,
            hasPreviousPage: last !== null ? startOffset > lowerBound : false,
            hasNextPage: first !== null ? endOffset < upperBound : false,
            count,
        },
    };
}
